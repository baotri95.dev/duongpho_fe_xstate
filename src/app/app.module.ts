import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DuongphoComponent } from './duongpho/duongpho.component';
import { HttpClientModule } from '@angular/common/http';
import { DuongPhoService } from 'src/app/duongpho/duongpho.service';
import { StateMachine } from 'src/app/xstate/xstate-store';
import { NgxsModule } from '@ngxs/store';
import { DPState } from 'src/app/biz/duongpho.state';
import { DPSelectors } from 'src/app/biz/duongpho-selectors';


@NgModule({
  declarations: [
    AppComponent,
    DuongphoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgxsModule.forFeature([DPState]),
    FormsModule, ReactiveFormsModule, HttpClientModule,
    HttpClientModule,
    NgxsModule.forRoot(),

  ],
  providers: [DuongPhoService, StateMachine, DPSelectors],
  bootstrap: [AppComponent]
})
export class AppModule { }
