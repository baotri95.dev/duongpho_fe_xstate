import { DuongPhoDto } from 'src/app/duongpho/duongpho.tdo';
// import { State } from 'xstate';
import { State, Action, StateContext } from '@ngxs/store';
import { StoreDuongPhoAction,
     AddDuongPhoAction,
      UpdateDuongPhoAction,
       DeleteDuongPhoAction, ChangeStateAction } from 'src/app/biz/duongpho-action';
import produce from 'immer';
import { Injectable } from '@angular/core';
export class DuongPhoModel {
    duongpho: DuongPhoDto[]; xstate: {message: string };

}
export function inintModel(): DuongPhoModel {
    return { duongpho: [], xstate: null };
}

@State<DuongPhoModel>({
    name: 'Duong_Pho',
    defaults: inintModel()
})
@Injectable(
)
export class DPState {
    @Action(StoreDuongPhoAction)
    storeDP(ctx: StateContext<DuongPhoModel>, action: StoreDuongPhoAction) {
        ctx.patchState(
            {
                duongpho: action.load
            }
        );
    }
    @Action(ChangeStateAction)
    xstate(ctx: StateContext<DuongPhoModel>, action: ChangeStateAction) {
        ctx.patchState(
            {
                xstate: action.load
            }
        );
    }
    @Action(AddDuongPhoAction)
    addDP(ctx: StateContext<DuongPhoModel>, action: AddDuongPhoAction) {
        const model = action.load;
        const state = ctx.getState().duongpho;
        const duongpho = produce(state, dr => {
            dr.push(model);
        });
        ctx.patchState({
            duongpho
        });
    }
    @Action(UpdateDuongPhoAction)
    updateDP(ctx: StateContext<DuongPhoModel>, action: UpdateDuongPhoAction) {
        const model = action.load;
        const state = ctx.getState().duongpho;
        const index = state.findIndex(d => d._id === model._id);
        state[index] = model;
        ctx.patchState({
            duongpho: state
        });
    }
    @Action(DeleteDuongPhoAction)
    deleteDP(ctx: StateContext<DuongPhoModel>, action: DeleteDuongPhoAction) {
        const model = action.load;
        const state = ctx.getState().duongpho;
        const duongpho = state.filter((d) => d._id !== model._id);
        ctx.patchState({
            duongpho
        });
    }
}
