import { DuongPhoDto } from 'src/app/duongpho/duongpho.tdo';

export class StoreDuongPhoAction {
    static readonly type = '[Duong Pho] Store data';
    constructor(public load: DuongPhoDto[]) {}
}
export class AddDuongPhoAction {
    static readonly type = '[Duong Pho] Add data';
    constructor(public load: DuongPhoDto) {}
}
export class UpdateDuongPhoAction {
    static readonly type = '[Duong Pho] Update data';
    constructor(public load: DuongPhoDto) {}
}
export class DeleteDuongPhoAction {
    static readonly type = '[Duong Pho] Delete data';
    constructor(public load: DuongPhoDto) {}
}
export class ChangeStateAction {
    static readonly type = 'Xstate';
    constructor(public load: {message: string}) {}
}
