import { Injectable } from '@angular/core';
import { Selector } from '@ngxs/store';
import { DuongPhoModel, DPState } from 'src/app/biz/duongpho.state';

@Injectable()
export class DPSelectors {

  @Selector([DPState])
  static duongpho$(state: DuongPhoModel): any[] {
    return state.duongpho;
  }
  @Selector([DPState])
  static xstate$(state: DuongPhoModel): any {
    return state.xstate;
  }
}
