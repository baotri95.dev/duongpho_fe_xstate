import { Injectable } from '@angular/core';
import { MachineOptions, AnyEventObject, Machine, interpret, EventObject, assign } from 'xstate';
import { of, Observable } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
import { DuongPhoService } from 'src/app/duongpho/duongpho.service';
import { Store, Select } from '@ngxs/store';
import {
    AddDuongPhoAction,
    UpdateDuongPhoAction, DeleteDuongPhoAction
    , ChangeStateAction,
    StoreDuongPhoAction
} from 'src/app/biz/duongpho-action';
import { DPSelectors } from 'src/app/biz/duongpho-selectors';
import { DuongPhoDto } from 'src/app/duongpho/duongpho.tdo';
@Injectable(
    {
        providedIn: 'root'
    }
)
// tslint:disable-next-line: no-unused-expression
export class StateMachine {
    public service;
    @Select(DPSelectors.duongpho$) readonly duongpho$: Observable<DuongPhoDto[]>;
    @Select(DPSelectors.xstate$) readonly xstate$: Observable<any>;
    constructor(private dpService: DuongPhoService, private store: Store) {
    }
    public machin = Machine({
        id: 'machin',
        initial: 'idle',
        states: {
            idle: {
                on: {
                    '': 'fetch'
                },
                meta: { message: 'idle' }
            },
            fetch: {
                invoke: {
                    // call server
                    src: (_, event) => this.dpService.getAll().pipe(
                        map(
                            (data) => {
                                this.store.dispatch(new StoreDuongPhoAction(data));
                                return { type: 'SUCCESS', data };
                            }
                        ),
                        catchError(
                            () => {
                                return of({ type: 'FAILD', data: 'fail' });
                            }
                        )
                    ),
                },
                on: {
                    CREATE: 'create',
                    UPDATE: 'update',
                    DELETE: 'delete_result',
                    SUCCESS: {
                        target: 'succeess'
                    },
                    FAILD: {
                        target: 'failure'
                    }
                },
                meta: { message: 'fetch...' }
            },
            loading: {
                on: {
                    CREATE: 'create',
                    UPDATE: 'update',
                    DELETE: 'delete_result'
                },
                meta: { message: 'loading...' }
            },
            create: {
                on: {
                    CREATE_RESULT: 'create_result',
                    UPDATE: 'update',
                    DELETE: 'delete_result'
                },
                meta: { message: 'create ready...' }
            },
            create_result: {
                invoke: {
                    src: (context, event: any) => this.dpService.create(event)
                        .pipe(
                            map(
                                (data) => {
                                    this.store.dispatch(new AddDuongPhoAction(data));
                                    return { type: 'SUCCESS', data };
                                }
                            ),
                            catchError(
                                () => {
                                    return of({ type: 'FAILD', data: 'fail' });
                                }
                            )
                        )
                },
                on: {
                    SUCCESS: {
                        target: 'succeess'
                    },
                    FAILD: {
                        target: 'failure'
                    }
                },
                meta: { message: 'create...' }
            },
            delete_result: {
                invoke: {
                    src: (context, event: any) => this.dpService.remove(event.id).pipe(
                        map(
                            (data) => {
                                this.store.dispatch(new DeleteDuongPhoAction(data));
                                return { type: 'SUCCESS', data };
                            }
                        ),
                        catchError(
                            () => {
                                return of({ type: 'FAILD', data: 'fail' });
                            }
                        )
                    )
                },
                on: {
                    SUCCESS: {
                        target: 'succeess'
                    },
                    FAILD: {
                        target: 'failure'
                    }
                },
                meta: { message: 'delete...' }
            },
            succeess: {
                on: {
                    CREATE: 'create',
                    UPDATE: 'update',
                    DELETE: 'delete_result'
                },
                meta: { message: 'succeess' }
            },
            failure: {
                on: {
                    CREATE: 'create',
                    UPDATE: 'update',
                    DELETE: 'delete_result'
                },
                meta: { message: 'faild' }
            },
            update: {
                on: {
                    CREATE: 'create',
                    UPDATE_RESULT: 'update_result',
                    DELETE: 'delete_result'
                },
                meta: { message: 'update ready...' }
            },
            update_result: {
                invoke: {
                    src: (context, event: any) => this.dpService.updatew(event.id, event.value).pipe(
                        map(
                            (data) => {
                                this.store.dispatch(new UpdateDuongPhoAction(data));
                                return { type: 'SUCCESS', data };
                            }
                        ),
                        catchError(
                            () => {
                                return of({ type: 'FAILD', data: 'fail' });
                            }
                        )
                    )
                },
                on: {
                    SUCCESS: {
                        target: 'succeess'
                    },
                    FAILD: {
                        target: 'failure'
                    }
                },
                meta: { message: 'update...' }
            },
        }
    });
    builds() {
        // this.promiseMachine = Machine<any, DuongPhoMachineSchema, TypeEvents>(DuongPhoMachineConfig).withConfig(this.XstateOptions);
        this.service = interpret(this.machin).onTransition((trs) => {
            this.store.dispatch(new ChangeStateAction(this.mergeMeta(trs.meta) as any));
            console.log(this.mergeMeta(trs.meta));
        }).start();
    }
    onTransition(type?: any, event?: any) {
        this.service.send(type, event);
    }
    mergeMeta(meta) {
        return Object.keys(meta).reduce((acc, key) => {
            const value = meta[key];
            Object.assign(acc, value);
            return acc;
        }, {});
    }

}
