import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DuongphoComponent } from './duongpho.component';

describe('DuongphoComponent', () => {
  let component: DuongphoComponent;
  let fixture: ComponentFixture<DuongphoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DuongphoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DuongphoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
