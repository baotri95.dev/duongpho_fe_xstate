export class DuongPhoDto {
    // tslint:disable-next-line: variable-name
    _id?: string;
    district: string;
    street: string;
    ward: string;
    city: string;
  }
