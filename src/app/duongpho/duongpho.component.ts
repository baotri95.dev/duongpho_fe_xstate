import { Component, OnInit } from '@angular/core';
import { DuongPhoDto } from 'src/app/duongpho/duongpho.tdo';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DuongPhoService } from 'src/app/duongpho/duongpho.service';
import { StateMachine } from 'src/app/xstate/xstate-store';

@Component({
  selector: 'app-duongpho',
  templateUrl: './duongpho.component.html',
  styleUrls: ['./duongpho.component.css']
})
export class DuongphoComponent implements OnInit {
  duongpho: DuongPhoDto[] = [];
  dpForm: FormGroup;
  isThem: boolean;
  title: string;
  id: string;
  state: any;
  constructor(private fb: FormBuilder, private xstate: StateMachine) { }

  ngOnInit() {
    this.xstate.builds();
    this.xstate.duongpho$.subscribe((res) => {
      this.duongpho = res;
    });
    this.xstate.xstate$.subscribe((res) => {
      this.state = res.message;
    });
    this.dpForm = this.inintForm(null);
  }

  inintForm(data) {
    const frm = this.fb.group({
      street: [data && data.street ? data.street : '', Validators.required],
      ward: [data && data.ward ? data.ward : '', Validators.required],
      city: [data && data.city ? data.city : '', Validators.required],
      district: [data && data.district ? data.district : '', Validators.required]
    });
    return frm;
  }
  ngClassSwitch(data) {
    switch (this.state) {
      case data: return 'btn btn-primary';
      default: return 'btn btn-outline-secondary';
    }
  }
  save() {
    this.dpForm.markAllAsTouched();
    if (this.dpForm.invalid) {
      alert('Lỗi, Nhập đầy đủ thông tin');
      return;
    }
    const value = this.dpForm.value;
    this.xstate.service.send('CREATE_RESULT', value);
    this.isThem = false;
  }
  updateBtn(modal, item) {
    this.title = 'Sửa thông tin';
    this.isThem = true;
    this.dpForm = this.inintForm(item);
    this.id = item._id;
    this.xstate.onTransition({type: 'UPDATE'}, {});
  }
  add() {
    this.dpForm = this.inintForm(null);
    this.title = 'Thêm thông tin';
    this.isThem = true;
    this.xstate.onTransition({type: 'CREATE'}, {});
  }
  saveUpdate() {
    this.dpForm.markAllAsTouched();
    if (this.dpForm.invalid) {
      alert('Lỗi');
      return;
    }
    const frm = this.dpForm.value;
    console.log({id: this.id, value: frm});
    this.xstate.onTransition('UPDATE_RESULT', {id: this.id, value: frm});
    this.isThem = false;
  }
  remove(item) {
    this.xstate.service.send('DELETE', {id: item._id});
    this.isThem = false;
  }
}
