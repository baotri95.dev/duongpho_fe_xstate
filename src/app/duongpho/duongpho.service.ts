import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DuongPhoDto } from 'src/app/duongpho/duongpho.tdo';
@Injectable()
export class DuongPhoService {
  constructor(private http: HttpClient) { }
  public Api = 'https://duong-pho.herokuapp.com/duongpho';
  getAll() {
    return this.http.get<DuongPhoDto[]>(this.Api);
  }
  create(body: DuongPhoDto) {
    const url = this.Api + '/save';
    return this.http.post<DuongPhoDto>(url, body);
  }
  updatew(id: string, body: DuongPhoDto) {
    console.log(id);
    const url = this.Api + `/${id}`;
    return this.http.put<DuongPhoDto>(url, body);
  }
  remove(id: string) {
    const url = this.Api + `/${id}`;
    return this.http.delete<DuongPhoDto>(url);
  }
}
