import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DuongphoComponent } from 'src/app/duongpho/duongpho.component';


const routes: Routes = [
  {
    path: '', component: DuongphoComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
